#!/usr/bin/env jruby

# Simple dummy UI for a Roguelike using VTerm and Swing.
#
# Copyright (C) 2023 Chris Reuter; BSD license; NO WARRANTY!


# First, we import the Jar file containing VTerm and its depencies:
proc {
  root = File.realpath(File.dirname(__FILE__))
  $CLASSPATH << File.join(root,'vterm_blob', 'target')
}.call()

require 'java'
require 'vterm_blob-1.0-SNAPSHOT-jar-with-dependencies.jar'


#
# Modules to provide a more Rubylike interface to the Java classes.
#

module VTerm
  module Component ; include_package "com.valkryst.VTerminal.component" ; end
  module Font ; include_package "com.valkryst.VTerminal.font" ; end
  module Image ; include_package "com.valkryst.VTerminal.image" ; end
  module Palette ; include_package "com.valkryst.VTerminal.palette" ; end
  module Plaf ; include_package "com.valkryst.VTerminal.plaf" ; end

  include_package "com.valkryst.VTerminal"
end

module Swing
  include_package "javax.swing"
  module Border
    include_package 'javax.swing.border'
  end
end

module Awt
  include_package "java.awt"
end


# DIY assertions; helpful for debugging.
def assert(&block)
  raise "Assertion failed!" unless block.call
end



# Listener class for keystrokes.  (A lot of the time, we can get away
# with using a Ruby block, but in this case, we only care about the
# keyTyped() event and want to discard the others.
class KeyHandler
  include java.awt.event.KeyListener
  def initialize(&action)
    @action = action
  end

  def keyTyped(e) = @action.call(e)

  def keyReleased(evt) = self
  def keyPressed(e) = self
end


# The main demo class.
class RogueDemo
  WIDTH = 80
  MAP_HEIGHT = 25
  LOG_HEIGHT = 4
  STAT_HEIGHT = 2
  POPUP_WIDTH = WIDTH / 3
  POPUP_HEIGHT = MAP_HEIGHT

  SPELLS = "Magic Missile
            Magic Bullet
            Magic Gathering
            Magic Mike
            Magic Dessert Topping
            Magic Floor Wax".split(/\n\s*/)

  CLOTHES ="Top Hat
            Spats
            Left Sock
            Cravate
            Cuff Links
            Ascott
            Monocle".split(/\n\s*/)



  def initialize
    @frame = nil            # The main window

    @log = nil              # The message pane above the map
    @map = nil              # The map
    @stats_board = nil      # The various game stats below the map

    @modal = nil            # nil or current pop-up control

    @player_pos = [4,6]

    @stats = {
      spells:       7,
      ac:           3,
      wc:           2,
      level:        5,
      time:         1,
      health:       22,
      gold:         581,
      str:          6,
      int:          16,
      wis:          16,
      dex:          6,
      cha:          8,
    }.then {|stats_hash|
      Struct.new(*stats_hash.keys).new(*stats_hash.values)
    }

    init_ui()
  end

  def init_ui
    # The main window
    @frame = Swing::JFrame.new("Game Thingy")

    # The map
    @map = VTerm::Component::VPanel.new(WIDTH, MAP_HEIGHT)
    @frame.add(@map, Awt::BorderLayout::CENTER)

    # The message pane.  (We could also use VTextArea here.)
    @log = Swing::JTextArea.new("Totally a real game!\nPress '?' for help",
                                LOG_HEIGHT, WIDTH)
    @log.setLineWrap(true)
    @log.setEditable(false)
    @frame.add(@log, Awt::BorderLayout::NORTH)

    @stats_board = VTerm::Component::VPanel.new(WIDTH, STAT_HEIGHT)
    @frame.add(@stats_board, Awt::BorderLayout::SOUTH)

    @frame.setDefaultCloseOperation(Swing::JFrame::EXIT_ON_CLOSE)
    @frame.setFocusable(true)
    @frame.pack
    @frame.setVisible(true)

    @frame.addKeyListener KeyHandler.new { |evt|
      do_key(evt.keyChar.chr)
    }

    set_ui_colours()

    update_map()
    update_stats()
  end

  def set_ui_colours
    @map.setBackground(Awt::Color.new(0.3, 0.1, 0.3))
    @log.setBackground(Awt::Color.new(0.1, 0.3, 0.3))
    @stats_board.setBackground(Awt::Color.new(0.3, 0.3, 0.1))
  end

  def msg(text)
    # JTextArea should scroll to the botton if I move the caret there,
    # but it *doesn't*.  So after three days of trying to get it to do
    # that one simple thing, I've given up and resorted to this.
    logtxt = @log.getText() + "\n" + text
    loglines = logtxt.split(/\n/)
    loglines = loglines[-LOG_HEIGHT .. -1] if loglines.size > LOG_HEIGHT
    logtxt = loglines.join("\n")
    @log.setText(logtxt)
  end

  def update_stats
    items = @stats.to_h.to_a
    middle = items.size / 2
    field_width = (WIDTH / middle) - 1

    fields = items.map do |namesym, value|
      name = namesym.to_s
      name = name.size > 3 ? name.capitalize : name.upcase
      valstr = value.to_s

      padding = field_width - (name.size + valstr.size + 2)
      name + ": " + (' ' * padding) + valstr
    end

    rows = [ fields[0.. middle - 1], fields[middle..] ]
    for y in 0..1
      row = rows[y].join(" ")
      for x in 0..([WIDTH, row.size].min - 1)
        @stats_board.setCodePointAt(x, y, row[x].ord)
      end
      @stats_board.repaint
    end
  end

  def do_key(key)
    if @modal
      # Should be unreachable, but...
      puts "waiting for picker to close."
      return
    end

    java.lang.System.exit(0) if key == 'q'

    if key == '?'
      show_help()
      return
    end

    if key == 'c'
      open_picker("Cast which spell?", SPELLS.dup) {|index, desc|
        if desc
          make_direction_select("Cast #{desc} which direction?") {|dir|
            if dir
              msg("You cast '#{desc}' in direction '#{dir}'.")
            else
              msg("Never mind.")
            end
          }
        else
          msg("Never mind.")
        end
      }
      return
    end

    if key == 'w'
      open_picker("Wear what?", CLOTHES.dup) {|index, desc|
        if desc
          msg("You try to wear the #{desc.downcase}, then think better of it.")
        else
          msg("Never mind; nudity suits you better anyway.")
        end
      }
      return
    end

    if key == 's'
      open_textbox("Say something:", "") {|msg|
        msg = msg.strip
        msg("\"#{msg}\"") unless msg.strip.empty?
      }
    end

    dirs = {
      h: [-1, 0],
      j: [0, 1],
      k: [0, -1],
      l: [1, 0],

      y: [-1, -1],
      u: [1, -1],
      b: [-1, 1],
      n: [1,1],
    }

    delta = dirs[key.intern]
    if delta
      x, y = @player_pos
      dx, dy = delta
      x += dx
      y += dy

      if x.between?(1, right - 1) && y.between?(1, bottom - 1)
        @player_pos = [x, y]
        update_map()
      else
        @stats.health -= 2
        msg(%w{Ouch! Ow! Bonk! Ooof! Thud!}.sample)
      end

      @stats.health += 1 unless @stats.health > 21
      @stats.time += 1
      update_stats()
    end
  end

  # Print a help message in @log
  def show_help
    msg("[hjklyubn] - movement (vi-style); " +
        "(c)ast, (s)ay, (w)ear, (q)uit; (?) help")
  end

  # Draw a plausible-looking map
  def update_map
    for y in 0 .. bottom()
      for x in 0 .. right()
        if [x, y] == @player_pos
          cp = '@'
        elsif (x == 0 || x == right) && (y == 0 || y == bottom)
          cp = '+'
        elsif x == 0 || x == right
          cp = '|'
        elsif y == 0 || y == bottom
          cp = '-'
        elsif x % 5 == 0 && y % 5 == 0
          cp = '.'
        else
          cp = ' '
        end

        @map.setCodePointAt(x, y, cp.ord)
      end
    end

    @map.repaint()
  end


  def open_textbox(prompt, initial = "", &action)
    assert { !@modal }

    @modal = Swing::JPanel.new(Awt::BorderLayout.new)

    # We put the actual controls into an inner JPanel which goes into
    # the NORTH slot of @modal.  This keeps the components together
    # and placed at the top of the pane.
    innerbox = Swing::JPanel.new(Awt::BorderLayout.new)

    heading = Swing::JTextArea.new(prompt, 3, POPUP_WIDTH)
    heading.setLineWrap(true)
    heading.setEditable(false)
    innerbox.add(heading, Awt::BorderLayout::NORTH)

    entry = VTerm::Component::VTextField.new(initial, POPUP_WIDTH - 2)
    innerbox.add(entry, Awt::BorderLayout::CENTER)

    footer, ok, cancel = make_footer()
    innerbox.add(footer, Awt::BorderLayout::SOUTH)

    @modal.add(innerbox, Awt::BorderLayout::NORTH)
    @frame.add(@modal, Awt::BorderLayout::EAST)

    closer = proc {|select|
      contents = entry.getText

      @frame.remove(@modal)
      @modal = nil
      @frame.revalidate

      action.call(contents) if select
    }

    ok.addActionListener { closer.call(true) }
    cancel.addActionListener { closer.call(false) }

    # Map escape and enter.  (Note that I use Ruby strings here; this
    # may not be portable and you should probably use the pre-defined
    # constants instead.)
    im = entry.getInputMap
    im.put(Swing::KeyStroke.getKeyStroke("\n".ord, 0), "enter")
    entry.getActionMap.put("enter") { closer.call(true) }
    im.put(Swing::KeyStroke.getKeyStroke("\e".ord, 0), "escape")
    entry.getActionMap.put("escape") { closer.call(false) }

    entry.requestFocusInWindow()
    @frame.revalidate
  end

  def open_picker(heading, items, &selection_block)
    assert { !@modal }

    items = items.to_java(:string)

    listbox = Swing::JList.new(items)
    listbox.setSelectedIndex(0)
    listbox.setFocusable(true)

    scroller = Swing::JScrollPane.new(listbox)
    @modal = Swing::JPanel.new(Awt::BorderLayout.new)
    @modal.add(scroller, Awt::BorderLayout::CENTER)
    scroller.setPreferredSize(tileDim(POPUP_WIDTH, MAP_HEIGHT - 5) )

    heading = Swing::JTextArea.new(heading, 3, 40)
    heading.setLineWrap(true)
    heading.setEditable(false)
    @modal.add(heading, Awt::BorderLayout::NORTH)

    footer, ok, cancel = make_footer("Select", "Cancel")
    @modal.add(footer, Awt::BorderLayout::SOUTH)

    @frame.add(@modal, Awt::BorderLayout::EAST)

    # Closer function; we use a proc so we don't repeat ourselves
    # but keep it local
    closer = proc {|ok|
      assert { @modal }
      if ok
        i, v = [listbox.getSelectedIndex, listbox.getSelectedValue.to_s]
      else
        i, v = [-1, nil]
      end

      @frame.remove(@modal)
      @modal = nil
      @frame.revalidate

      selection_block.call(i, v)
    }

    # Callbacks
    ok.addActionListener { closer.call(true) }
    cancel.addActionListener { closer.call(false) }
    listbox.addMouseListener {|evt|
      closer.call(true) if evt.getClickCount == 2
    }
    listbox.addKeyListener KeyHandler.new {|evt|
      closer.call(true) if evt.keyChar.chr == "\n"
      closer.call(false) if evt.keyChar.chr == "\e"   # escape
    }

    listbox.requestFocusInWindow()
    @frame.revalidate
  end

  private

  def make_footer(oktxt = "OK", canceltxt = "Cancel")
    footer = Swing::Box.createHorizontalBox
    ok = Swing::JButton.new(oktxt)
    footer.add(ok)
    footer.add(Swing::Box.createHorizontalGlue)
    cancel = Swing::JButton.new(canceltxt)
    footer.add(cancel)

    return [footer, ok, cancel]
  end

  public

  def make_direction_select(prompt, &action)
    msg = <<~'EOF'

         y  k  u
          \ | /
           \|/
         h-----l
           /|\
          / | \
         b  j  n

    (or ESC to cancel)

    EOF
    msg = prompt + "\n" + msg

    make_one_keystroke_selector(msg, "hjklyubn", &action)
  end

  private

  def make_one_keystroke_selector(message, keys, &action)
    assert { !@modal }

    @modal = Swing::JPanel.new(Awt::BorderLayout.new)

    widget = Swing::JTextArea.new(message, MAP_HEIGHT, POPUP_WIDTH)
    widget.setLineWrap(true)
    widget.setEditable(false)


    widget.addKeyListener KeyHandler.new {|evt|
      kc = evt.keyChar.chr
      if kc == "\e" || keys.include?(kc)
        @frame.remove(@modal)
        @modal = nil
        @frame.revalidate

        kc = nil unless keys.include?(kc)
        action.call(kc)
      end
    }
    @modal.add(widget, Awt::BorderLayout::CENTER)
    @modal.setFocusable(true)

    @frame.add(@modal, Awt::BorderLayout::EAST)
    @frame.revalidate
    widget.requestFocusInWindow
  end

  def right = @map.widthInTiles - 1
  def bottom = @map.heightInTiles - 1

  def tileSized(x, y)
    i = VTerm::Plaf::VTerminalLookAndFeel.getInstance
    return [x*i.getTileWidth, y*i.getTileHeight]
  end

  def tileDim(x,y) = Awt::Dimension.new(*tileSized(x, y))
end


def main
  Swing::UIManager.setLookAndFeel(
    VTerm::Plaf::VTerminalLookAndFeel.getInstance(24)
  )

  Swing::SwingUtilities.invokeLater { RogueDemo.new }
end

main()
