# A VTerm Roguelike UI Demo in JRuby

This is a small example program demonstrating a user interface for a
hypothetical Roguelike game using Swing and
[VTerminal](https://github.com/Valkryst/VTerminal) in pure JRuby.

You are encouraged to copy this code and use it as the starting point
for your own Roguelike game.

Here's a screenshot:

![Screenshot of the UI](screenshot.png)


## Setting it up

You'll need:

1. Java 16 or later plus the corresponding JDK.
2. Maven.
3. JRuby 9.4.x with `rake` installed.

You'll need to compile the dependency JAR like this:

    rake

If your default JVM can't run Maven (not that I'd have any bitter
experience of that or anything), you can specify an alternate with
`MVN_JAVA_HOME` on the command line.

    rake MVN_JAVA_HOME=<path-to-jvm>

And if it can't doesn't support Java 16, you can specify another JDK
for compiling with `BUILD_JDK`:

    rake BUILD_JDK=<path-to-jvm>

These can be combined as needed.

If the build succeeds, it will open up a small window showing a little
text with VTerm.

After this, you should be able to start it up with:

    jruby ui_demo.rb


## How it Works

The directory `vterm_blob` holds a trivial Java program (the one
`rake` runs to show a successful build) and the corresponding Maven
build file (`pom.xml`).  Running `rake` will invoke `mvn` to build it.

Maven automatically fetches all of the dependencies needed (including
VTerminal) and packages them in one `jar` file, which we then use
from JRuby.



